cc=g++
flags=-O2 -Wall
output=bfc

all:
	$(cc) main.cpp $(flags) -o $(output)

clean:
	rm ./$(output)

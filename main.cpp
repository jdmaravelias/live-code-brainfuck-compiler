#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <exception>
/*
This program, bfc, is an easy-to-read modern C++ brainfuck-to-C compiler.
Copyright (C) 2016 Hedon d'Ennui AKA Joshua Maravelias

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

CFG BNF
program ::= <stmts>
stmts   ::= <stmt><stmts>
         |  EMPTY
stmt    ::= <cmds>
         |  [ <stmts> ]
cmds    ::= <cmd><cmds>
         |  EMPTY
cmd     ::= +
         |  -
         |  >
         |  <
         |  .
         |  ,
*/

using namespace std;

enum Token
{
  RIGHT, LEFT,  //> < move pointer
  PLUS,  MINUS, //+ - increment/decrement cell under pointer
  OUT,   IN,    //. , output and input
  START, STOP,  //[ ] control flow
  EOT           //end of input
};
class Lexer
{
  string cache_;
  string::iterator next_char_;
  public:
    Lexer(string infile)
    {
      ifstream read(infile);
      if(!read)
        throw runtime_error("Canno open file");

      for(string input; getline(read,input);)
        cache_ += input;
      
      next_char_ = cache_.begin();
    }
    Token next_token()
    {
      while(next_char_ != cache_.end())
      {
        switch(*next_char_)
        {
#define C(X,Y) case X: ++next_char_; return Y
          C('>',RIGHT); C('<',LEFT); C('+',PLUS);  C('-',MINUS);
          C('.',OUT);   C(',',IN);   C('[',START); C(']',STOP);
#undef C
          default:
            ++next_char_;
        }
      }
      return EOT;//bye felicia
    }
};
class Parser
{
  Lexer lexie_;
  ofstream output_;
  Token curr_;
  void match(Token t)
  {
    if(curr_ == t)
      curr_ = lexie_.next_token();
    else
      throw runtime_error("ur grammar sux & u sux");
  }
  void program()
  {
    output_ << "#include <stdlib.h>\n#include <stdio.h>\n#include <string.h>"
               "\n\nint main(int argc, char** argv)\n{\n"
               "\tunsigned char *space = valloc(sizeof *space*30000);\n"
               "\tmemset(space,0,sizeof *space*30000);\n"
               "\tint ptr = 0;\n\n";
    stmts();
    match(EOT);
    output_ << "\treturn 0;\n}\n";
  }
  void stmts()
  {
    if(curr_ == EOT || curr_ == STOP)
      ;
    else
    {
      stmt();
      stmts();
    }
  }
  void stmt()
  {
    if(curr_ == START)
    {
      match(START);
      output_ << "\twhile(space[ptr])\n\t{\n";
      stmts();
      output_ << "\t}\n";
      match(STOP);
    }else if(curr_ == STOP)
      throw runtime_error("is no brack");
    else
      cmds();
  }
  void cmds()
  {
    switch(curr_)
    {
      case RIGHT:case LEFT:case PLUS:case MINUS: case OUT: case IN: 
        cmd();cmds();
      default:;
    }
  }
  void cmd()
  {
    switch(curr_)
    {
#define C(T,S) case T: match(T); output_ << "\t"S";\n"; break
      C(RIGHT,  "++ptr");
      C(LEFT,   "--ptr");
      C(PLUS,   "++space[ptr]");
      C(MINUS,  "--space[ptr]");
      C(OUT,    "putc(space[ptr],stdout)");
      C(IN,     "space[ptr] = getc(stdin)");
#undef C
      default: throw runtime_error("cmd is err");
    }
  }
public:
  Parser(string infile, string outfile) :
    lexie_(infile), output_(outfile,ios::trunc), curr_(lexie_.next_token())
  {
    if(!output_)
      throw runtime_error("u canno use that file for ur c");
  }
  void parse()
  { program(); }
};
int main(int argc, char** argv)
{
  if(argc != 3)
  {
    cerr << "Your args suck\n";
    return -1;
  }
  
  try
  {
    Parser dolly_parson(argv[1],argv[2]);
    dolly_parson.parse();
  }catch(exception& e)
  {
    cerr << e.what() << endl;
    return -1;
  }
  return 0;
}
